/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/*
 * Copyright (C) 2000-2001 Eazel, Inc
 *               2001 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Ian McKellar <ian@eazel.com>
 */

/* nautilus-gtkhtml-view.c - gtkhtml content view component.
 * It displays HTML pages inside Nautilus
 */

#include <config.h>
#include "nautilus-gtkhtml-view.h"

#include <bonobo/bonobo-i18n.h>
#include <bonobo/bonobo-zoomable.h>
#include <libgnome/gnome-macros.h>
#include <string.h>

#include <libgnomevfs/gnome-vfs.h>

#include <libgtkhtml/document/htmldocument.h>
#include <libgtkhtml/graphics/htmlpainter.h>
#include <libgtkhtml/gtkhtmlcontext.h>
#include <libgtkhtml/util/htmlstream.h>
#include <libgtkhtml/view/htmlview.h>

#define d(x)

struct NautilusGtkHTMLViewDetails {
	GnomeVFSURI *base;
	HtmlDocument *doc;
	HtmlView *view;
	BonoboZoomable *zoomable;
	gfloat zoom_level;
	/*
	GtkWidget		  *menu;
	*/
};

BONOBO_CLASS_BOILERPLATE (NautilusGtkHTMLView, nautilus_gtkhtml_view,
			  NautilusView, NAUTILUS_TYPE_VIEW)


static float preferred_zoom_levels[] = {
	0.2, 0.4, 0.6, 0.8,
	1.0, 1.2, 1.4, 1.6, 1.8,
	2.0, 2.2, 2.4, 2.6, 2.8,
	3.0, 3.2, 3.4, 3.6, 3.8,
	4.0, 4.2, 4.4, 4.6, 4.8,
	5.0, 5.2, 5.4, 5.6, 5.8,
	6.0, 6.2, 6.4, 6.6, 6.8,
	7.0, 7.2, 7.4, 7.6, 7.8,
	8.0, 8.2, 8.4, 8.6, 8.8,
	9.0, 9.2, 9.4, 9.6, 9.8,
};

static const gchar *preferred_zoom_level_names[] = {
	"20%", "40%", "60%", "80%",
	"100%", "120%", "140%", "160%", "180%",
	"200%", "220%", "240%", "260%", "280%",
	"300%", "320%", "340%", "360%", "380%",
	"400%", "420%", "440%", "460%", "480%",
	"500%", "520%", "540%", "560%", "580%",
	"600%", "620%", "640%", "660%", "680%",
	"700%", "720%", "740%", "760%", "780%",
	"800%", "820%", "840%", "860%", "880%",
	"900%", "920%", "940%", "960%", "980%",
};
#define NUM_ZOOM_LEVELS (sizeof (preferred_zoom_levels) / sizeof (float))

static void nautilus_gtkhtml_view_finalize (GObject *object);

static void url_requested (HtmlDocument *doc, const gchar *uri,
                           HtmlStream *stream, gpointer data);

static void gtkhtml_load_location_callback (NautilusView *nautilus_view,
					    const char *location,
					    gpointer user_data);
static void gtkhtml_stop_loading_callback (NautilusGtkHTMLView *nautilus_view,
					   gpointer user_data);

#ifdef USELESS
static void gtkhtml_merge_bonobo_items_callback (BonoboControl *control,
						 gboolean state,
						 gpointer user_data);
#endif

#define BUFFER_SIZE 8192

typedef struct {
	NautilusGtkHTMLView *view;
	HtmlDocument *doc;
	HtmlStream *stream;
	GnomeVFSAsyncHandle *handle;
} StreamData;

static void
title_changed (HtmlDocument *doc, const gchar *new_title, gpointer data)
{
	d(g_print("title_changed: %s\n", new_title));
	g_return_if_fail (NAUTILUS_IS_VIEW (data));

	nautilus_view_set_title (NAUTILUS_VIEW (data), new_title);
}

static void
set_base (HtmlDocument *document,
          const gchar *url,
	  NautilusGtkHTMLView *view)
{
	d(g_print("set_base: %s\n", url));
	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (view));

	if (view->details->base) {
		GnomeVFSURI *new_uri;

		new_uri = gnome_vfs_uri_resolve_relative (view->details->base, url);
		gnome_vfs_uri_unref (view->details->base);
		view->details->base = new_uri;
	}
	else
		view->details->base = gnome_vfs_uri_new (url);
}

static void
free_stream_data (StreamData *sdata, gboolean remove)
{
	GSList *connection_list;

	if (remove) {
		connection_list = g_object_get_data (G_OBJECT (sdata->doc), "connection_list");
		connection_list = g_slist_remove (connection_list, sdata);
		g_object_set_data (G_OBJECT (sdata->doc), "connection_list", connection_list);
		if (connection_list == NULL)
			nautilus_view_report_load_complete (NAUTILUS_VIEW (sdata->view));
	}
	html_stream_close (sdata->stream);

	g_free (sdata);
}

static void
stream_cancel (HtmlStream *stream, gpointer user_data, gpointer cancel_data)
{
	StreamData *sdata = (StreamData *)cancel_data;
	gnome_vfs_async_cancel (sdata->handle);
	free_stream_data (sdata, TRUE);
}

static void
vfs_close_callback (GnomeVFSAsyncHandle *handle,
                    GnomeVFSResult result,
                    gpointer callback_data)
{
}

static void
vfs_read_callback (GnomeVFSAsyncHandle *handle, GnomeVFSResult result,
                   gpointer buffer, GnomeVFSFileSize bytes_requested,
                   GnomeVFSFileSize bytes_read, gpointer callback_data)
{
	StreamData *sdata = (StreamData *)callback_data;

	if (result != GNOME_VFS_OK) {
		gnome_vfs_async_close (handle, vfs_close_callback, sdata);
		free_stream_data (sdata, TRUE);
		g_free (buffer);
	} else {
		html_stream_write (sdata->stream, buffer, bytes_read);

		gnome_vfs_async_read (handle, buffer, bytes_requested,
				      vfs_read_callback, sdata);
	}
}

static void
vfs_open_callback (GnomeVFSAsyncHandle *handle, GnomeVFSResult result,
                   gpointer callback_data)
{
	StreamData *sdata = (StreamData *)callback_data;

	if (result != GNOME_VFS_OK) {

		g_warning ("Open failed: %s.\n", gnome_vfs_result_to_string (result));
		free_stream_data (sdata, TRUE);
	} else {
		gchar *buffer;

		buffer = g_malloc (BUFFER_SIZE);
		gnome_vfs_async_read (handle, buffer, BUFFER_SIZE, vfs_read_callback, sdata);
	}
}

static void
vfs_load (NautilusGtkHTMLView *view, GnomeVFSURI *vfs_uri, HtmlStream *stream)
{
	StreamData *sdata;
	GSList *connection_list;

	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (view));

	sdata = g_new0 (StreamData, 1);
	sdata->view = view;
	sdata->doc = view->details->doc;
	sdata->stream = stream;

	connection_list = g_object_get_data (G_OBJECT (sdata->doc),
	                                     "connection_list");
	connection_list = g_slist_prepend (connection_list, sdata);
	g_object_set_data (G_OBJECT (sdata->doc), "connection_list",
	                   connection_list);

	gnome_vfs_async_open_uri (&sdata->handle, vfs_uri, GNOME_VFS_OPEN_READ,
				  GNOME_VFS_PRIORITY_DEFAULT,
				  vfs_open_callback, sdata);

	html_stream_set_cancel_func (stream, stream_cancel, sdata);
}

static void
navigate (NautilusGtkHTMLView *view, const gchar *url, gboolean new_window)
{
	char *location;
	d(g_print("navigate: %s\n", url));

	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (view));

	if (view->details->base) {
		GnomeVFSURI *vfs_uri;
		vfs_uri = gnome_vfs_uri_resolve_relative (view->details->base, url);
		location = gnome_vfs_uri_to_string (vfs_uri, GNOME_VFS_URI_HIDE_NONE);
		gnome_vfs_uri_unref (vfs_uri);
	}
	else
		location = g_strdup (url);

	if (new_window) {
		nautilus_view_open_location (NAUTILUS_VIEW (view), location,
					     Nautilus_ViewFrame_OPEN_ACCORDING_TO_MODE,
					     0, NULL);
	} else {
		nautilus_view_open_location (NAUTILUS_VIEW (view), location,
					     Nautilus_ViewFrame_OPEN_IN_NAVIGATION,
					     0, NULL);
	}

	g_free (location);
}

static void
link_clicked (HtmlDocument *doc, const gchar *url, gpointer data)
{
	d(g_print("link_clicked: %s\n", url));
	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (data));

	navigate (NAUTILUS_GTKHTML_VIEW (data), url, FALSE);
}

#if 0
static int
button_press_event (HtmlView *html,
		    GdkEventButton *event,
		    NautilusGtkHTMLView *view)
{
	g_return_if_fail (HTML_IS_VIEW (html));
	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (view));

	g_print ("[YAK] event->button = %d\n", event->button);
	g_print ("[YAK] event->state = %d\n", event->state);
	g_print ("[YAK] html->pointer_url = %d\n", html->pointer_url);

	if (event->button == 2 && html->pointer_url != NULL) {
		/* open in new window */
		navigate (view, html->pointer_url, TRUE);
		return TRUE;
	} else if (event->button == 1 && (event->state & GDK_SHIFT_MASK) &&
			html->pointer_url != NULL) {
		g_print ("[YAK] download `%s'", html->pointer_url);
		/* download ()
		return TRUE;*/
		return FALSE;
	} else if (event->button == 3 && html->pointer_url != NULL) {
		g_print ("[YAK] popup...\n");

		gtk_menu_popup (GTK_MENU (view->details->menu), NULL, NULL,
				NULL, NULL, event->button, event->time);

	} else {
		return FALSE;
	}
}

static void
on_url (GtkHTML *html,
	  const gchar *url,
	  NautilusGtkHTMLView *view)
{
	char *location;
	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (view));

	if (url == NULL) {
		nautilus_view_report_status (NAUTILUS_VIEW (view), "");
	} else {
		location = resolve_relative_uri (view, url);
		nautilus_view_report_status (NAUTILUS_VIEW (view), location);
		g_free (location);
	}
}
#endif

static void
submit (HtmlDocument *document, const gchar *action, const gchar *method,
        const gchar *encoding, gpointer data)
{
	NautilusGtkHTMLView *view;
	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (data));
	view = NAUTILUS_GTKHTML_VIEW (data);

	if (!method || strcasecmp(method, "GET") == 0) {
		char *real_action;
		real_action = g_strdup_printf ("%s?%s", action,
		                               encoding);
		navigate (NAUTILUS_GTKHTML_VIEW (view), real_action, FALSE);
		g_free (real_action);
	} else {
		GtkWidget *dialog;
		dialog = gtk_message_dialog_new (NULL, 0,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_OK,
						 _("Form method %s not supported."),
						 action);
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
	}
}

static void
kill_old_connections (HtmlDocument *doc)
{
	GSList *connection_list, *tmp;

	tmp = connection_list = g_object_get_data (G_OBJECT (doc), "connection_list");
	while(tmp) {
		StreamData *sdata = (StreamData *)tmp->data;
		gnome_vfs_async_cancel (sdata->handle);
		free_stream_data (sdata, FALSE);

		tmp = tmp->next;
	}
	g_object_set_data (G_OBJECT (doc), "connection_list", NULL);
	g_slist_free (connection_list);
}

static void
load_location (NautilusGtkHTMLView *view,
	       const char *location)
{
	gchar *str_url;
	const gchar *anchor;
	GnomeVFSURI *old_base = NULL;

	d(g_print("load_location: %s\n", location));

	if (view->details->base)
		old_base = gnome_vfs_uri_ref (view->details->base);

	set_base (view->details->doc, location, view);

	if (old_base && gnome_vfs_uri_equal (old_base, view->details->base)) {
		/* Same document, just jump to the anchor */
		anchor = gnome_vfs_uri_get_fragment_identifier (view->details->base);
		if (anchor != NULL)
			html_view_jump_to_anchor (view->details->view, anchor);

		gnome_vfs_uri_unref (old_base);
		return;
	}

	kill_old_connections (view->details->doc);

	html_document_clear (view->details->doc);
	html_document_open_stream (view->details->doc, "text/html");
	gtk_adjustment_set_value (gtk_layout_get_vadjustment (GTK_LAYOUT (view->details->view)), 0);

	vfs_load (view, view->details->base, view->details->doc->current_stream);

	if (view->details->base) {
		anchor = gnome_vfs_uri_get_fragment_identifier (view->details->base);
		if (anchor != NULL)
			html_view_jump_to_anchor (view->details->view, anchor);
	}
}

static void
url_requested (HtmlDocument *doc, const gchar *url,
               HtmlStream *stream, gpointer data)
{
	GnomeVFSURI *vfs_uri;
	NautilusGtkHTMLView *view = NAUTILUS_GTKHTML_VIEW (data);

	d(g_print("url_requested: %s\n", url));
	g_return_if_fail (url != NULL);
	g_return_if_fail (stream != NULL);

	if (view->details->base)
		vfs_uri = gnome_vfs_uri_resolve_relative (view->details->base, url);
	else
		vfs_uri = gnome_vfs_uri_new (url);

	vfs_load (view, vfs_uri, stream);
	gnome_vfs_uri_unref (vfs_uri);
}

static void
gtkhtml_stop_loading_callback (NautilusGtkHTMLView *nautilus_view,
                               gpointer user_data)
{
	kill_old_connections (nautilus_view->details->doc);
}

static void
gtkhtml_load_location_callback (NautilusView *nautilus_view,
			       const char *location,
			       gpointer user_data)
{
	NautilusGtkHTMLView *view;

	d(g_print("gtkhtml_load_location_callback: %s\n", location));

	g_return_if_fail (NAUTILUS_IS_VIEW (nautilus_view));
	g_return_if_fail (location != NULL);

	view = NAUTILUS_GTKHTML_VIEW (nautilus_view);

	nautilus_view_report_load_underway (NAUTILUS_VIEW (view));

	load_location (view, location);
}

static void
bonobo_gtkhtml_callback (BonoboUIComponent *ui,
			gpointer           user_data,
			const char        *verb)
{
 	NautilusGtkHTMLView *view;
	GtkWidget *dialog;
	char *label_text;
	char *location;

	g_return_if_fail (BONOBO_IS_UI_COMPONENT (ui));
        g_return_if_fail (verb != NULL);

	view = NAUTILUS_GTKHTML_VIEW (user_data);

	location = gnome_vfs_uri_to_string (view->details->base, GNOME_VFS_URI_HIDE_NONE);

	if (strcmp (verb, "GtkHTML Menu Item") == 0) {
		label_text = g_strdup_printf ("%s\n\nYou selected the GtkHTML menu item.",
					      location);
	} else {
		g_return_if_fail (strcmp (verb, "GtkHTML Dock Item") == 0);
		label_text = g_strdup_printf (_("%s\n\nYou clicked the GtkHTML toolbar button."),
					      location);
	}

	g_print (label_text);

	g_free (location);
	g_free (label_text);
}

#ifdef USELESS
static void
gtkhtml_merge_bonobo_items_callback (BonoboControl *control,
				    gboolean       state,
				    gpointer       user_data)
{
 	NautilusGtkHTMLView *view;
	BonoboUIComponent *ui_component;
	BonoboUIVerb verbs [] = {
		BONOBO_UI_VERB ("GtkHTML Menu Item", bonobo_gtkhtml_callback),
		BONOBO_UI_VERB ("GtkHTML Dock Item", bonobo_gtkhtml_callback),
		BONOBO_UI_VERB_END
	};

	g_return_if_fail (BONOBO_IS_CONTROL (control));

	view = NAUTILUS_GTKHTML_VIEW (user_data);

	if (state) {
		ui_component = nautilus_view_set_up_ui (NAUTILUS_VIEW (view),
							DATADIR,
							"nautilus-gtkhtml-view-ui.xml",
							"nautilus-gtkhtml-view");

		bonobo_ui_component_add_verb_list_with_data (ui_component, verbs, view);
	}
}
#endif

#if 0
static void
menu_open_link (GtkWidget *widget,
		NautilusGtkHTMLView *view) {
	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (view));
	navigate (view, GTK_HTML (view->details->gtkhtml)->pointer_url, FALSE);
}

static void
menu_open_link_new_window (GtkWidget *widget,
		           NautilusGtkHTMLView *view) {
	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (view));
	navigate (view, GTK_HTML (view->details->gtkhtml)->pointer_url, TRUE);
}

static void
menu_download_link (GtkWidget *widget,
		    NautilusGtkHTMLView *view) {
	g_return_if_fail (NAUTILUS_IS_GTKHTML_VIEW (view));
	g_print ("menu_download_link (%s)\n",
			GTK_HTML (view->details->gtkhtml)->pointer_url);
}
#endif

static int
zoom_index_from_float (float zoom_level)
{
	int i;

	for (i = 0; i < NUM_ZOOM_LEVELS - 1; i++) {
		float this, epsilon;

		/* if we're close to a zoom level */
		this = preferred_zoom_levels [i];
		epsilon = this * 0.01;

		if (zoom_level < this+epsilon)
			return i;
	}

	return NUM_ZOOM_LEVELS - 1;
}

static float
zoom_level_from_index (int index)
{
	if (index > NUM_ZOOM_LEVELS - 1)
		index = NUM_ZOOM_LEVELS - 1;

	return preferred_zoom_levels [index];
}

static void
zoomable_zoom_in_callback (BonoboZoomable *zoomable, 
			   NautilusGtkHTMLViewDetails *details)
{
	float this_zoom_level, new_zoom_level;
	int index;

	g_return_if_fail (details != NULL);

	index = zoom_index_from_float (details->zoom_level);
	if (index == NUM_ZOOM_LEVELS - 1)
		return;

	/* if we were zoomed to fit, we're not on one of the pre-defined level.
	 * We want to zoom into the next real level instead of skipping it
	 */
	this_zoom_level = zoom_level_from_index (index);
	
	if (this_zoom_level > details->zoom_level) {
		new_zoom_level = this_zoom_level;
	} else {  
		index++;
		new_zoom_level = zoom_level_from_index (index);
	}
	
	g_signal_emit_by_name (zoomable, "set_zoom_level",
				 new_zoom_level);
}

static void
zoomable_zoom_out_callback (BonoboZoomable *zoomable, 
			    NautilusGtkHTMLViewDetails *details)
{
	float new_zoom_level;
	int index;

	g_return_if_fail (details != NULL);

	index = zoom_index_from_float (details->zoom_level);
	if (index == 0)
		return;

	index--;
	new_zoom_level = zoom_level_from_index (index);

	g_signal_emit_by_name (zoomable, "set_zoom_level",
				 new_zoom_level);
}

static void
zoomable_zoom_to_fit_callback (BonoboZoomable *zoomable, 
			       NautilusGtkHTMLViewDetails *details)
{
	g_signal_emit_by_name (zoomable, "set_zoom_level", 1.0);
}

static void
zoomable_zoom_to_default_callback (BonoboZoomable *zoomable, 
				   NautilusGtkHTMLViewDetails *details)
{
	g_signal_emit_by_name (zoomable, "set_zoom_level", 1.0);
}

static void
zoomable_set_zoom_level_callback (BonoboZoomable *zoomable, 
				  float new_zoom_level,
				  NautilusGtkHTMLViewDetails *details)
{
	g_return_if_fail (details != NULL);

	html_view_set_magnification (details->view, new_zoom_level);
	details->zoom_level = new_zoom_level;

	bonobo_zoomable_report_zoom_level_changed (details->zoomable, 
						   new_zoom_level, NULL);
}


static void
nautilus_gtkhtml_view_instance_init (NautilusGtkHTMLView *view)
{
	GtkWidget *sw;
	GtkWidget *item;

	view->details = g_new0 (NautilusGtkHTMLViewDetails, 1);

	view->details->doc = html_document_new ();

	g_signal_connect (G_OBJECT (view->details->doc), "request_url",
	                  G_CALLBACK (url_requested), view);

	g_signal_connect (G_OBJECT (view->details->doc), "title_changed",
	                  G_CALLBACK (title_changed), view);

	g_signal_connect (G_OBJECT (view->details->doc), "set_base",
			  G_CALLBACK (set_base), view);

	g_signal_connect (G_OBJECT (view->details->doc), "link_clicked",
			  G_CALLBACK (link_clicked), view);

	g_signal_connect (G_OBJECT (view->details->doc), "submit",
			  G_CALLBACK (submit), view);

	view->details->view = HTML_VIEW (html_view_new ());
	html_view_set_document (view->details->view, view->details->doc);

	sw = gtk_scrolled_window_new (gtk_layout_get_hadjustment (GTK_LAYOUT (view->details->view)),
				       gtk_layout_get_vadjustment (GTK_LAYOUT (view->details->view)));
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
	                                GTK_POLICY_AUTOMATIC,
	                                GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (sw), GTK_WIDGET (view->details->view));
	gtk_widget_show_all (sw);

	nautilus_view_construct (NAUTILUS_VIEW (view), sw);

	g_signal_connect (G_OBJECT (view), "load_location",
			  G_CALLBACK (gtkhtml_load_location_callback), NULL);

	g_signal_connect (G_OBJECT (view), "stop_loading",
			  G_CALLBACK (gtkhtml_stop_loading_callback), NULL);

#ifdef USELESS
        g_signal_connect (G_OBJECT (nautilus_view_get_bonobo_control (NAUTILUS_VIEW (view))),
                          "activate",
                          G_CALLBACK (gtkhtml_merge_bonobo_items_callback),
                          view);
#endif

	view->details->zoomable = bonobo_zoomable_new ();

	g_signal_connect (view->details->zoomable, "set_zoom_level",
			    G_CALLBACK (zoomable_set_zoom_level_callback), view->details);
	g_signal_connect (view->details->zoomable, "zoom_in",
			    G_CALLBACK (zoomable_zoom_in_callback), view->details);
	g_signal_connect (view->details->zoomable, "zoom_out",
			    G_CALLBACK (zoomable_zoom_out_callback), view->details);
	g_signal_connect (view->details->zoomable, "zoom_to_fit",
			    G_CALLBACK (zoomable_zoom_to_fit_callback), view->details);
	g_signal_connect (view->details->zoomable, "zoom_to_default",
			    G_CALLBACK (zoomable_zoom_to_default_callback), view->details);

	view->details->zoom_level = 1.0;
	bonobo_zoomable_set_parameters_full (view->details->zoomable,
					     view->details->zoom_level,
					     preferred_zoom_levels [0],
					     preferred_zoom_levels [NUM_ZOOM_LEVELS - 1],
					     FALSE, FALSE, TRUE,
					     preferred_zoom_levels,
					     preferred_zoom_level_names,
					     NUM_ZOOM_LEVELS);


	bonobo_object_add_interface (BONOBO_OBJECT (view),
				     BONOBO_OBJECT (view->details->zoomable));



#if 0
	view->details->menu = gtk_menu_new ();

	item = gtk_menu_item_new_with_label (_("Open Link"));
	g_signal_connect (G_OBJECT (item), "activate",
			  G_CALLBACK (menu_open_link), view);
	gtk_widget_show (item);
	gtk_menu_append (GTK_MENU (view->details->menu), item);

	item = gtk_menu_item_new_with_label
		(_("Open Link in New Window"));
	g_signal_connect (G_OBJECT (item), "activate",
			  G_CALLBACK (menu_open_link_new_window), view);
	gtk_widget_show (item);
	gtk_menu_append (GTK_MENU (view->details->menu), item);

	item = gtk_menu_item_new_with_label (_("Download Link"));
	g_signal_connect (G_OBJECT (item), "activate",
			  G_CALLBACK (menu_download_link), view);
	//gtk_widget_show (item);
	gtk_menu_append (GTK_MENU (view->details->menu), item);
#endif

	/*
	g_signal_connect (G_OBJECT (view->details->view), "on_url",
			  G_CALLBACK (on_url), view);
	g_signal_connect (G_OBJECT (view->details->gtkhtml), "redirect",
			  G_CALLBACK (redirect), view);
	g_signal_connect (G_OBJECT (view->details->gtkhtml),
			  "button-press-event",
			  G_CALLBACK (button_press_event), view);
	*/
}

static void
nautilus_gtkhtml_view_finalize (GObject *object)
{
	NautilusGtkHTMLView *view;

	view = NAUTILUS_GTKHTML_VIEW (object);

	gnome_vfs_uri_unref (view->details->base);

	/* We shouldn't really need to call html_document_clear() but
	   libgtkhtml seems to have a bug with reference counting otherwise. */
	kill_old_connections (view->details->doc);
	html_document_clear (view->details->doc);
	g_object_unref (G_OBJECT (view->details->doc));

#if 0
	if (view->details->menu) {
		gtk_widget_unref (view->details->menu);
	}
#endif

	g_free (view->details);

	GNOME_CALL_PARENT (G_OBJECT_CLASS, finalize, (object));
}

static void
nautilus_gtkhtml_view_class_init (NautilusGtkHTMLViewClass *class)
{
	G_OBJECT_CLASS (class)->finalize = nautilus_gtkhtml_view_finalize;
}
