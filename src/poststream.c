/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2000-2001 Eazel, Inc
 *               2001 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Ian McKellar <ian@eazel.com>
 */

#include <config.h>
#include "nautilus-gtkhtml-content-view.h"

void
poststream_post (NautilusGtkHTMLContentView *view, 
		const char *location, const char *post_data,
		GtkHTMLStream *html_stream)
{
	NautilusGtkHTMLPostStream *stream;

	g_assert (NAUTILUS_IS_GTKHTML_CONTENT_VIEW (view));
	g_assert (location != NULL);

	stream = g_new0 (NautilusGtkHTMLPostStream, 1);
	stream->html_stream = html_stream;
	stream->view = view;
	stream->location = g_strdup (location);
	stream->ghttp_handle = ghttp_request_new();
	ghttp_set_uri (stream->ghttp_handle, stream->location);
	ghttp_set_type (stream->ghttp_handle, ghttp_type_post);
	ghttp_set_sync (stream->ghttp_handle, ghttp_sync);
	ghttp_set_body (stream->ghttp_handle, (char *)post_data, strlen (post_data));
	/* FIXME - set proxy info from gconf */
	ghttp_prepare (stream->ghttp_handle);

	ghttp_process (stream->ghttp_handle);

	/* FIXME - check that this is really an html doc */
	
	gtk_html_write (GTK_HTML (stream->view->details->gtkhtml),
			stream->html_stream, 
			ghttp_get_body (stream->ghttp_handle),
			ghttp_get_body_len (stream->ghttp_handle));

	gtk_html_end (GTK_HTML (stream->view->details->gtkhtml), 
			stream->html_stream, GTK_HTML_STREAM_OK);

	ghttp_request_destroy (stream->ghttp_handle);

	g_free (stream->location);

	nautilus_view_report_load_complete (NAUTILUS_VIEW (stream->view));

	g_free (stream);
}

